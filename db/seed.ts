import { genSalt, hash } from 'bcryptjs'
import { prisma } from '../src/context/prisma'

const main = async () => {
  const password = await hash('123456', await genSalt())

  const owner = await prisma.createUser({
    email: 'owner@mail.com',
    name: 'Owner 1',
    password,
  })
  const coach = await prisma.createUser({
    email: 'coach@mail.com',
    name: 'Coach 1',
    password,
  })
  const student = await prisma.createUser({
    email: 'student@mail.com',
    name: 'Student 1',
    password,
  })

  await prisma.createCompany({
    courses: {
      create: [
        {
          description:
            'Онлайн-практикум по созданию бизнеса с нуля и выведению его в прибыль за 60 дней',
          name: 'busness-start',
          roles: {
            create: [
              {
                rules: {
                  create: [{ name: 'COURSE_EDIT' }, { name: 'COURSE_DELETE' }],
                },
                title: 'Test course coaches',
                users: {
                  connect: [{ id: coach.id }],
                },
              },
              {
                rules: {
                  create: [{ name: 'COURSE_VIEW' }],
                },
                title: 'Students (Standard)',
                users: {
                  connect: [{ id: student.id }],
                },
              },
            ],
          },
          title: 'Buisness Start',
        },
      ],
    },
    name: 'webtrening',
    roles: {
      create: [
        {
          rules: {
            create: [{ name: 'COMPANY_OWNER' }],
          },
          title: 'Owners',
          users: {
            connect: [{ id: owner.id }],
          },
        },
        {
          rules: {
            create: [{ name: 'COMPANY_COURSE_CREATE' }],
          },
          title: 'Coaches',
          users: {
            connect: [{ id: coach.id }],
          },
        },
      ],
    },
    title: 'Webtrening',
  })

  const dancingCrabsCourses = [1, 2, 3, 4, 5, 6].map(index => ({
    name: `test-course-${index}`,
    roles: {
      create: [
        {
          rules: {
            create: [{ name: 'COURSE_EDIT' }, { name: 'COURSE_DELETE' }],
          },
          title: `Test course ${index} coaches`,
          users: {
            connect: [{ id: coach.id }],
          },
        },
      ],
    },
    title: `Test course ${index}`,
  }))

  await prisma.createCompany({
    courses: {
      create: dancingCrabsCourses as any,
    },
    name: 'dancing-crabs',
    roles: {
      create: [
        {
          rules: {
            create: [{ name: 'COMPANY_OWNER' }],
          },
          title: 'Owners',
          users: {
            connect: [{ id: owner.id }],
          },
        },
      ],
    },
    title: 'Dancing Crabs',
  })
}

// tslint:disable-next-line no-console
main().then(() => console.log('Success seeding 🎉'))
