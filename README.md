## Tools/Features

- **[Prisma GraphQL](https://www.prisma.io/)** (MySQL, TypeScript, seed data)
- **Arguments validation** (validator.js)
- **Permissions** (graphql-shield)
- **Error handling** (apollo-errors)
- **Lint** (tslint, prettier)
