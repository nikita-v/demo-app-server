export default {
  UserRole: {
    __resolveType: ({ courseId }: any) =>
      courseId ? 'CourseRole' : 'CompanyRole',
  },
}
