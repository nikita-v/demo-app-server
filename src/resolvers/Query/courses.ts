import { IContext } from '../../context'

const fragment = `
  fragment witCompanies on Course {
    id
    name
    title
    description
    company {
      id
      name
      title
    }
  }
`

export default async (_: any, _1: any, { userId, db }: IContext) =>
  db
    .courses({
      where: {
        OR: [
          {
            company: {
              roles_some: {
                AND: [
                  { rules_some: { name: 'COMPANY_OWNER' } },
                  { users_some: { id: userId } },
                ],
              },
            },
          },
          {
            roles_some: {
              AND: [
                {
                  rules_some: {
                    name_in: ['COURSE_VIEW', 'COURSE_EDIT', 'COURSE_DELETE'],
                  },
                },
                { users_some: { id: userId } },
              ],
            },
          },
        ],
      },
    })
    .$fragment(fragment)
