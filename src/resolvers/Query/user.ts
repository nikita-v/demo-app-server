import { IContext } from '../../context'

interface IUser {
  id: string
  profile: {
    name: string
  }
  roles: {
    id: string
    title?: string
    companyId?: string
    courseId?: string
    rules: {
      id: string
      name: string
    }
  }
}

const fragment = `
  fragment withRoles on User {
    id
    name
    roles {
      id
      title
      course {
        id
      }
      company {
        id
      }
      rules {
        id
        name
      }
    }
  }
`

export default async (
  _: any,
  args: {},
  { userId, db }: IContext,
): Promise<IUser> => {
  const { id, name, roles }: any = await db
    .user({ ...args, id: userId })
    .$fragment(fragment)

  return {
    id,
    profile: { name },
    roles: roles.map((v: any) => ({
      companyId: v.company ? v.company.id : undefined,
      courseId: v.course ? v.course.id : undefined,
      id: v.id,
      rules: v.rules,
      title: v.id,
    })),
  }
}
