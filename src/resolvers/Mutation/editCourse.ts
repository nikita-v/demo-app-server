import { IContext } from '../../context'

interface IArgs {
  companyId: string
  courseId: string
  courseName?: string
  courseTitle?: string
  courseDescription?: string
}

interface ICourse {
  id: string
  name: string
  title: string
  description?: string
}

export default async (
  _: any,
  args: IArgs,
  { db }: IContext,
): Promise<ICourse> =>
  db.updateCourse({
    data: {
      description: args.courseDescription,
      name: args.courseName,
      title: args.courseTitle,
    },
    where: { id: args.courseId },
  })
