import { IContext } from '../../context'

interface IArgs {
  companyId: string
  courseId: string
}

export default async (
  _: any,
  args: IArgs,
  { db }: IContext,
): Promise<boolean> => !!db.deleteCourse({ id: args.courseId })
