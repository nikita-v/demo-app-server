import { compare } from 'bcryptjs'
import { sign } from 'jsonwebtoken'

import { IContext } from '../../context'

interface IArgs {
  loginEmail: string
  loginPassword?: string
  temporaryCode?: string
}

interface ILoginData {
  token: string
  user: {
    id: string
    profile: { name: string }
    roles: {
      id: string
      title?: string
      companyId?: string
      courseId?: string
      rules: {
        id: string
        name: string
      }
    }
  }
}

export default async (
  _: any,
  args: IArgs,
  ctx: IContext,
): Promise<ILoginData> => {
  if (!args.loginPassword && !args.temporaryCode) {
    throw new Error('Password or code should be specified')
  }

  const user = await ctx.db.user({ email: args.loginEmail })

  if (args.temporaryCode && !(await compare(args.temporaryCode, user.code))) {
    throw new Error('Code is wrong')
  } else if (!(await compare(args.loginPassword, user.password))) {
    throw new Error('Password is wrong')
  }

  if (args.temporaryCode) {
    await ctx.db.updateUser({ where: { id: user.id }, data: { code: null } })
  }

  const fragment = `
    fragment withRules on Role {
      id
      title
      course {
        id
      }
      company {
        id
      }
      rules {
        id
        name
      }
    }
  `

  const roles: any = await ctx.db
    .user({ id: user.id })
    .roles()
    .$fragment(fragment)

  return {
    token: await sign({ id: user.id }, process.env.JWT_SECRET),
    user: {
      id: user.id,
      profile: {
        name: user.name,
      },
      roles: roles.map((v: any) => ({
        companyId: v.company ? v.company.id : undefined,
        courseId: v.course ? v.course.id : undefined,
        id: v.id,
        rules: v.rules,
        title: v.id,
      })),
    },
  }
}
