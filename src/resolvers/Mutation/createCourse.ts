import { IContext } from '../../context'

interface IArgs {
  companyId: string
  courseName: string
  courseTitle: string
  courseDescription: string
}

interface ICourse {
  id: string
  name: string
  title: string
  description: string
  company: {
    id: string
    name: string
    title: string
  }
}

const fragment = `
  fragment withCompany on Course {
    id
    name
    title
    description
    company {
      id
      name
      title
    }
  }
`

export default async (
  _: any,
  args: IArgs,
  { db }: IContext,
): Promise<ICourse> =>
  db
    .createCourse({
      company: { connect: { id: args.companyId } },
      description: args.courseDescription,
      name: args.courseName,
      title: args.courseTitle,
    })
    .$fragment(fragment)
