import interfaces from './interfaces'
import * as Mutation from './Mutation'
import * as Query from './Query'

export default {
  ...interfaces,
  Mutation,
  Query,
}
