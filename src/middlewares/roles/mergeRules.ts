import { IRole } from './'

export default (roles: IRole[]) =>
  roles
    // Merge roles
    .reduce((all, current) => all.concat(current.rules), [])
    // Merge rules
    .reduce((all, { name, ...options }) => {
      const index = all.findIndex((v: any) => v.name === name)

      return index > -1 ? all : all.concat({ name, ...options })

      // Merge options

      // for (const option in options) {
      //   options[option] = options[option].map(({ id }: any) => id)
      // }

      // if (index > -1) {
      //   for (const option in options) {
      //     all[index][option] = Array.from(
      //       new Set(all[index][option].concat(...options[option])),
      //     )
      //   }
      //
      //   return all
      // }

      // return all.concat({ name, ...options })
    }, [])
