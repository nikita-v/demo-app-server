import { IContext, IRule } from '../../context'

import mergeRules from './mergeRules'

export interface IRole {
  rules: IRule[]
}

interface IArgs {
  companyId?: string
  courseId?: string
}

export default async (
  resolve: any,
  root: any,
  args: IArgs,
  context: IContext,
  info: any,
) => {
  const rules = context.rules

  if (context.userId) {
    const rolesFragment = `
      fragment WithRules on Role {
        rules {
          name
        }
      }
    `

    // InsideCompanyRoles
    if (args.companyId) {
      const companyRoles: IRole[] = await context.db
        .user({
          id: context.userId,
        })
        .roles({ where: { company: { id: args.companyId } } })
        .$fragment(rolesFragment)

      rules.company = mergeRules(companyRoles)
    }

    // InsideCourseRoles
    if (args.courseId) {
      const courseRoles: IRole[] = await context.db
        .user({
          id: context.userId,
        })
        .roles({ where: { course: { id: args.courseId } } })
        .$fragment(rolesFragment)

      rules.course = mergeRules(courseRoles)
    }
  }

  return resolve(root, args, { ...context, rules }, info)
}
