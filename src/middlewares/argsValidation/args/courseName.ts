import { isAlpha, isEmpty, isLength, matches } from 'validator'

import { IArgError } from '../'
import { IContext } from '../../../context'

export default async (
  value: string,
  args: { companyId: string },
  { db }: IContext,
): Promise<IArgError[] | void> => {
  const errors: IArgError[] = []

  if (isEmpty(value)) {
    return errors.concat({ message: 'IS_EMPTY' })
  }

  if (!isLength(value, { max: 32 })) {
    errors.push({ message: 'LENGTH_MAX', options: { value: 32 } })
  }

  if (!isAlpha(value[0])) {
    errors.push({ message: 'FIRST_SYMBOL_NOT_LETTER' })
  }

  if (matches(value, /^([0-9-]+)$/) || !matches(value, /^([a-z0-9-]+)$/)) {
    errors.push({ message: 'NOT_ALPHANUMERIC' })
  }

  if (errors.length > 0) {
    return errors
  }

  if (
    await db.$exists.course({ name: value, company: { id: args.companyId } })
  ) {
    return errors.concat({ message: 'NOT_UNIQUE' })
  }
}
