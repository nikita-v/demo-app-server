import { isEmail } from 'validator'

import { IArgError } from '../'
import { IContext } from '../../../context'

export default async (
  value: string,
  _: any,
  { db }: IContext,
): Promise<IArgError[] | void> => {
  const errors: IArgError[] = []

  if (!isEmail(value)) {
    return errors.concat({ message: 'EMAIL_WRONG' })
  }

  if (!(await db.$exists.user({ email: value }))) {
    return errors.concat({ message: 'EMAIL_NOT_FOUND' })
  }
}
