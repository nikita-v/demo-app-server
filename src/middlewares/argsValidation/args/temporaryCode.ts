import { isLength } from 'validator'

import { IArgError } from '../'

export default async (value: string): Promise<IArgError[] | void> => {
  const errors: IArgError[] = []

  if (!isLength(value, { min: 4, max: 4 })) {
    return errors.concat({ message: 'LENGTH', options: { value: 4 } })
  }
}
