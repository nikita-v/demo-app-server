import { isEmpty, isLength } from 'validator'

import { IArgError } from '../'

export default async (value: string): Promise<IArgError[] | void> => {
  const errors: IArgError[] = []

  if (isEmpty(value)) {
    return errors.concat({ message: 'IS_EMPTY' })
  }

  if (!isLength(value, { max: 32 })) {
    errors.push({ message: 'LENGTH_MAX', options: { value: 32 } })
  }

  if (errors.length > 0) {
    return errors
  }
}
