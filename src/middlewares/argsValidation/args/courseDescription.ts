import { isLength } from 'validator'

import { IArgError } from '../'

export default async (value: string): Promise<IArgError[] | void> => {
  const errors: IArgError[] = []

  if (!isLength(value, { max: 260 })) {
    errors.push({ message: 'LENGTH_MAX', options: { value: 260 } })
  }

  if (errors.length > 0) {
    return errors
  }
}
