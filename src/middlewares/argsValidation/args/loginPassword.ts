import { isLength } from 'validator'

import { IArgError } from '../'

export default async (value: string): Promise<IArgError[] | void> => {
  const errors: IArgError[] = []

  if (!isLength(value, { min: 6, max: 64 })) {
    return errors.concat({ message: 'LENGTH', options: { min: 6, max: 64 } })
  }
}
