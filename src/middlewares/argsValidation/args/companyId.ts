import { isEmpty } from 'validator'

import { IArgError } from '../'
import { IContext } from '../../../context'

export default async (
  value: string,
  _: any,
  { db }: IContext,
): Promise<IArgError[] | void> => {
  const errors: IArgError[] = []

  if (isEmpty(value)) {
    return errors.concat({ message: 'IS_EMPTY' })
  }

  if (!(await db.$exists.company({ id: value }))) {
    return errors.concat({ message: 'COMPANY_NOT_FOUND' })
  }
}
