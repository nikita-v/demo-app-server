import { ApolloError } from 'apollo-errors'

import * as args from './args'

interface IArgs {
  [key: string]: any
}

export interface IArgError {
  message: string
  options?: IArgs
}

interface IArgsErrors {
  [key: string]: IArgError[]
}

export default async (resolve: any, ...params: any) => {
  const argsErrors: IArgsErrors = {}

  for (const name in params[1]) {
    if (!args.hasOwnProperty(name)) {
      throw new ApolloError(
        'ARG_NOT_EXISTED',
        {
          data: { name, value: params[1][name] },
          message: 'User sent not existed arg',
        },
        { message: 'ARG_NOT_EXISTED', data: { name } },
      )
    }

    const argErrors = await args[name](params[1][name], params[1], params[2])

    if (argErrors) {
      argsErrors[name] = argErrors
    }
  }

  if (Object.keys(argsErrors).length > 0) {
    throw new ApolloError(
      'ARGS_VALIDATION_ERROR',
      { message: 'User sent invalid arguments', data: argsErrors },
      { message: 'ARGS_VALIDATION_ERROR', data: argsErrors },
    )
  }

  return resolve(...params)
}
