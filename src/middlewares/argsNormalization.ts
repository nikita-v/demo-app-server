import { normalizeEmail, stripLow, trim } from 'validator'

export default (resolve: any, root: any, args: any, ...params: any) => {
  const normalizeArgs: { [key: string]: string } = {}

  for (const name in args) {
    let value = typeof args[name] === 'string' ? stripLow(trim(args[name])) : ''

    if (['email'].includes(name)) {
      value = normalizeEmail(value) || value
    }

    normalizeArgs[name] = value
  }

  return resolve(root, normalizeArgs, ...params)
}
