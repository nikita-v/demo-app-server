import { and, or, shield } from 'graphql-shield'

import * as rules from './rules'

export default shield({
  Mutation: {
    createCourse: and(
      rules.isAuth,
      or(rules.isAbleToCreateCourse, rules.isCompanyOwner),
    ),
    deleteCourse: and(
      rules.isAuth,
      or(rules.isAbleToDeleteCourse, rules.isCompanyOwner),
    ),
    editCourse: and(
      rules.isAuth,
      or(rules.isAbleToEditCourse, rules.isCompanyOwner),
    ),
  },
  Query: {
    courses: rules.isAuth,
    user: rules.isAuth,
  },
})
