import { rule } from 'graphql-shield'

import { IContext } from '../../../context'

export default rule()(
  async (_, _1, { rules }: IContext) =>
    rules.course.some(({ name }) => name === 'COURSE_EDIT') ||
    new Error('You have not access to edit this course'),
)
