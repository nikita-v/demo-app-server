import { rule } from 'graphql-shield'

export default rule()(
  async (_, _1, context) =>
    !!context.userId || new Error('You have not authorised'),
)
