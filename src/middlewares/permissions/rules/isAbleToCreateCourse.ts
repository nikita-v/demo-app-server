import { rule } from 'graphql-shield'

import { IContext } from '../../../context'

export default rule()(
  async (_, _1, { rules }: IContext) =>
    rules.company.some(({ name }) => name === 'COMPANY_COURSE_CREATE') ||
    new Error('You have not access to create courses'),
)
