import { rule } from 'graphql-shield'

import { IContext } from '../../../context'

export default rule()(
  async (_, _1, { rules }: IContext) =>
    rules.company.some(({ name }) => name === 'COMPANY_OWNER') ||
    new Error('You are not a company owner'),
)
