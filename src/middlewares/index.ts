import argsNormalization from './argsNormalization'
import argsValidation from './argsValidation'
import permissions from './permissions'
import roles from './roles'

export default [argsNormalization, argsValidation, roles, permissions]
