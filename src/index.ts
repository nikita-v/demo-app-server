import { config } from 'dotenv'
import { GraphQLServer } from 'graphql-yoga'

config()

import context from './context'
import formatError from './formatError'
import middlewares from './middlewares'
import resolvers from './resolvers'
import typeDefs from './typeDefs'

const server = new GraphQLServer({
  context,
  middlewares,
  resolvers,
  typeDefs,
})

server.start({
  formatError,
})
