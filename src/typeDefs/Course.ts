export default `
  extend type Query {
    courses: [Course]
  }

  extend type Mutation {
    createCourse(companyId: ID!, courseName: String!, courseTitle: String!, courseDescription: String!): Course
    editCourse(companyId: ID!, courseId: ID!, courseName: String, courseTitle: String, courseDescription: String): Course
    deleteCourse(companyId: ID!, courseId: ID!): Boolean
  }

  type Course {
    id: ID!
    name: String!
    title: String!
    description: String
    company: CourseCompany
  }

  type CourseCompany {
    id: ID!
    name: String!
    title: String!
  }
`
