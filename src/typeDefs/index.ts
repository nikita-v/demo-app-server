import Course from './Course'
import User from './User'

const Root = `
  type Query {
    _: Boolean
  }

  type Mutation {
    _: Boolean
  }
`

export default [Root, Course, User]
