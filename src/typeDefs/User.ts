export default `
  extend type Query {
    user: User
  }

  extend type Mutation {
    login(loginEmail: String!, loginPassword: String, temporaryCode: String): LoginData
  }

  type LoginData {
    token: String!
    user: User
  }

  type User {
    id: ID!
    profile: UserProfile!
    roles: [UserRole]
  }

  type UserProfile {
    name: String!
  }

  interface UserRole {
    id: ID!
    title: String!
    rules: [Rule]!
  }

  type CompanyRole implements UserRole {
    id: ID!
    title: String!
    rules: [Rule]!
    companyId: String!
  }

  type CourseRole implements UserRole {
    id: ID!
    title: String!
    rules: [Rule]!
    courseId: String!
  }

  type Rule {
    id: ID!
    name: String!
  }
`
