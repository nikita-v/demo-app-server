import getUserId from './getUserId'
import { prisma as db, Prisma as IPrisma } from './prisma'

export interface IRule {
  name: string
}

export interface IContextRules {
  company: IRule[]
  course: IRule[]
}

export interface IContext {
  db: IPrisma
  userId?: string
  rules: IContextRules
}

export default async ({ request }: any): Promise<IContext> => ({
  db,
  rules: { company: [], course: [] },
  userId: await getUserId(request.get('token')),
})
