import { verify } from 'jsonwebtoken'

export default async (token?: string): Promise<string | null> => {
  try {
    const { id } = (await verify(token, process.env.JWT_SECRET)) as {
      id: string
    }

    return id
  } catch (err) {
    return null
  }
}
