export const typeDefs = /* GraphQL */ `type AggregateCompany {
  count: Int!
}

type AggregateCourse {
  count: Int!
}

type AggregateRole {
  count: Int!
}

type AggregateRule {
  count: Int!
}

type AggregateUser {
  count: Int!
}

type BatchPayload {
  count: Long!
}

type Company {
  id: ID!
  name: String!
  title: String!
  roles(where: RoleWhereInput, orderBy: RoleOrderByInput, skip: Int, after: String, before: String, first: Int, last: Int): [Role!]
  courses(where: CourseWhereInput, orderBy: CourseOrderByInput, skip: Int, after: String, before: String, first: Int, last: Int): [Course!]
}

type CompanyConnection {
  pageInfo: PageInfo!
  edges: [CompanyEdge]!
  aggregate: AggregateCompany!
}

input CompanyCreateInput {
  name: String!
  title: String!
  roles: RoleCreateManyWithoutCompanyInput
  courses: CourseCreateManyWithoutCompanyInput
}

input CompanyCreateOneWithoutCoursesInput {
  create: CompanyCreateWithoutCoursesInput
  connect: CompanyWhereUniqueInput
}

input CompanyCreateOneWithoutRolesInput {
  create: CompanyCreateWithoutRolesInput
  connect: CompanyWhereUniqueInput
}

input CompanyCreateWithoutCoursesInput {
  name: String!
  title: String!
  roles: RoleCreateManyWithoutCompanyInput
}

input CompanyCreateWithoutRolesInput {
  name: String!
  title: String!
  courses: CourseCreateManyWithoutCompanyInput
}

type CompanyEdge {
  node: Company!
  cursor: String!
}

enum CompanyOrderByInput {
  id_ASC
  id_DESC
  name_ASC
  name_DESC
  title_ASC
  title_DESC
  createdAt_ASC
  createdAt_DESC
  updatedAt_ASC
  updatedAt_DESC
}

type CompanyPreviousValues {
  id: ID!
  name: String!
  title: String!
}

type CompanySubscriptionPayload {
  mutation: MutationType!
  node: Company
  updatedFields: [String!]
  previousValues: CompanyPreviousValues
}

input CompanySubscriptionWhereInput {
  mutation_in: [MutationType!]
  updatedFields_contains: String
  updatedFields_contains_every: [String!]
  updatedFields_contains_some: [String!]
  node: CompanyWhereInput
  AND: [CompanySubscriptionWhereInput!]
  OR: [CompanySubscriptionWhereInput!]
  NOT: [CompanySubscriptionWhereInput!]
}

input CompanyUpdateInput {
  name: String
  title: String
  roles: RoleUpdateManyWithoutCompanyInput
  courses: CourseUpdateManyWithoutCompanyInput
}

input CompanyUpdateManyMutationInput {
  name: String
  title: String
}

input CompanyUpdateOneWithoutCoursesInput {
  create: CompanyCreateWithoutCoursesInput
  update: CompanyUpdateWithoutCoursesDataInput
  upsert: CompanyUpsertWithoutCoursesInput
  delete: Boolean
  disconnect: Boolean
  connect: CompanyWhereUniqueInput
}

input CompanyUpdateOneWithoutRolesInput {
  create: CompanyCreateWithoutRolesInput
  update: CompanyUpdateWithoutRolesDataInput
  upsert: CompanyUpsertWithoutRolesInput
  delete: Boolean
  disconnect: Boolean
  connect: CompanyWhereUniqueInput
}

input CompanyUpdateWithoutCoursesDataInput {
  name: String
  title: String
  roles: RoleUpdateManyWithoutCompanyInput
}

input CompanyUpdateWithoutRolesDataInput {
  name: String
  title: String
  courses: CourseUpdateManyWithoutCompanyInput
}

input CompanyUpsertWithoutCoursesInput {
  update: CompanyUpdateWithoutCoursesDataInput!
  create: CompanyCreateWithoutCoursesInput!
}

input CompanyUpsertWithoutRolesInput {
  update: CompanyUpdateWithoutRolesDataInput!
  create: CompanyCreateWithoutRolesInput!
}

input CompanyWhereInput {
  id: ID
  id_not: ID
  id_in: [ID!]
  id_not_in: [ID!]
  id_lt: ID
  id_lte: ID
  id_gt: ID
  id_gte: ID
  id_contains: ID
  id_not_contains: ID
  id_starts_with: ID
  id_not_starts_with: ID
  id_ends_with: ID
  id_not_ends_with: ID
  name: String
  name_not: String
  name_in: [String!]
  name_not_in: [String!]
  name_lt: String
  name_lte: String
  name_gt: String
  name_gte: String
  name_contains: String
  name_not_contains: String
  name_starts_with: String
  name_not_starts_with: String
  name_ends_with: String
  name_not_ends_with: String
  title: String
  title_not: String
  title_in: [String!]
  title_not_in: [String!]
  title_lt: String
  title_lte: String
  title_gt: String
  title_gte: String
  title_contains: String
  title_not_contains: String
  title_starts_with: String
  title_not_starts_with: String
  title_ends_with: String
  title_not_ends_with: String
  roles_every: RoleWhereInput
  roles_some: RoleWhereInput
  roles_none: RoleWhereInput
  courses_every: CourseWhereInput
  courses_some: CourseWhereInput
  courses_none: CourseWhereInput
  AND: [CompanyWhereInput!]
  OR: [CompanyWhereInput!]
  NOT: [CompanyWhereInput!]
}

input CompanyWhereUniqueInput {
  id: ID
  name: String
}

type Course {
  id: ID!
  company: Company
  roles(where: RoleWhereInput, orderBy: RoleOrderByInput, skip: Int, after: String, before: String, first: Int, last: Int): [Role!]
  name: String!
  title: String!
  description: String
}

type CourseConnection {
  pageInfo: PageInfo!
  edges: [CourseEdge]!
  aggregate: AggregateCourse!
}

input CourseCreateInput {
  company: CompanyCreateOneWithoutCoursesInput
  roles: RoleCreateManyWithoutCourseInput
  name: String!
  title: String!
  description: String
}

input CourseCreateManyWithoutCompanyInput {
  create: [CourseCreateWithoutCompanyInput!]
  connect: [CourseWhereUniqueInput!]
}

input CourseCreateOneWithoutRolesInput {
  create: CourseCreateWithoutRolesInput
  connect: CourseWhereUniqueInput
}

input CourseCreateWithoutCompanyInput {
  roles: RoleCreateManyWithoutCourseInput
  name: String!
  title: String!
  description: String
}

input CourseCreateWithoutRolesInput {
  company: CompanyCreateOneWithoutCoursesInput
  name: String!
  title: String!
  description: String
}

type CourseEdge {
  node: Course!
  cursor: String!
}

enum CourseOrderByInput {
  id_ASC
  id_DESC
  name_ASC
  name_DESC
  title_ASC
  title_DESC
  description_ASC
  description_DESC
  createdAt_ASC
  createdAt_DESC
  updatedAt_ASC
  updatedAt_DESC
}

type CoursePreviousValues {
  id: ID!
  name: String!
  title: String!
  description: String
}

input CourseScalarWhereInput {
  id: ID
  id_not: ID
  id_in: [ID!]
  id_not_in: [ID!]
  id_lt: ID
  id_lte: ID
  id_gt: ID
  id_gte: ID
  id_contains: ID
  id_not_contains: ID
  id_starts_with: ID
  id_not_starts_with: ID
  id_ends_with: ID
  id_not_ends_with: ID
  name: String
  name_not: String
  name_in: [String!]
  name_not_in: [String!]
  name_lt: String
  name_lte: String
  name_gt: String
  name_gte: String
  name_contains: String
  name_not_contains: String
  name_starts_with: String
  name_not_starts_with: String
  name_ends_with: String
  name_not_ends_with: String
  title: String
  title_not: String
  title_in: [String!]
  title_not_in: [String!]
  title_lt: String
  title_lte: String
  title_gt: String
  title_gte: String
  title_contains: String
  title_not_contains: String
  title_starts_with: String
  title_not_starts_with: String
  title_ends_with: String
  title_not_ends_with: String
  description: String
  description_not: String
  description_in: [String!]
  description_not_in: [String!]
  description_lt: String
  description_lte: String
  description_gt: String
  description_gte: String
  description_contains: String
  description_not_contains: String
  description_starts_with: String
  description_not_starts_with: String
  description_ends_with: String
  description_not_ends_with: String
  AND: [CourseScalarWhereInput!]
  OR: [CourseScalarWhereInput!]
  NOT: [CourseScalarWhereInput!]
}

type CourseSubscriptionPayload {
  mutation: MutationType!
  node: Course
  updatedFields: [String!]
  previousValues: CoursePreviousValues
}

input CourseSubscriptionWhereInput {
  mutation_in: [MutationType!]
  updatedFields_contains: String
  updatedFields_contains_every: [String!]
  updatedFields_contains_some: [String!]
  node: CourseWhereInput
  AND: [CourseSubscriptionWhereInput!]
  OR: [CourseSubscriptionWhereInput!]
  NOT: [CourseSubscriptionWhereInput!]
}

input CourseUpdateInput {
  company: CompanyUpdateOneWithoutCoursesInput
  roles: RoleUpdateManyWithoutCourseInput
  name: String
  title: String
  description: String
}

input CourseUpdateManyDataInput {
  name: String
  title: String
  description: String
}

input CourseUpdateManyMutationInput {
  name: String
  title: String
  description: String
}

input CourseUpdateManyWithoutCompanyInput {
  create: [CourseCreateWithoutCompanyInput!]
  delete: [CourseWhereUniqueInput!]
  connect: [CourseWhereUniqueInput!]
  set: [CourseWhereUniqueInput!]
  disconnect: [CourseWhereUniqueInput!]
  update: [CourseUpdateWithWhereUniqueWithoutCompanyInput!]
  upsert: [CourseUpsertWithWhereUniqueWithoutCompanyInput!]
  deleteMany: [CourseScalarWhereInput!]
  updateMany: [CourseUpdateManyWithWhereNestedInput!]
}

input CourseUpdateManyWithWhereNestedInput {
  where: CourseScalarWhereInput!
  data: CourseUpdateManyDataInput!
}

input CourseUpdateOneWithoutRolesInput {
  create: CourseCreateWithoutRolesInput
  update: CourseUpdateWithoutRolesDataInput
  upsert: CourseUpsertWithoutRolesInput
  delete: Boolean
  disconnect: Boolean
  connect: CourseWhereUniqueInput
}

input CourseUpdateWithoutCompanyDataInput {
  roles: RoleUpdateManyWithoutCourseInput
  name: String
  title: String
  description: String
}

input CourseUpdateWithoutRolesDataInput {
  company: CompanyUpdateOneWithoutCoursesInput
  name: String
  title: String
  description: String
}

input CourseUpdateWithWhereUniqueWithoutCompanyInput {
  where: CourseWhereUniqueInput!
  data: CourseUpdateWithoutCompanyDataInput!
}

input CourseUpsertWithoutRolesInput {
  update: CourseUpdateWithoutRolesDataInput!
  create: CourseCreateWithoutRolesInput!
}

input CourseUpsertWithWhereUniqueWithoutCompanyInput {
  where: CourseWhereUniqueInput!
  update: CourseUpdateWithoutCompanyDataInput!
  create: CourseCreateWithoutCompanyInput!
}

input CourseWhereInput {
  id: ID
  id_not: ID
  id_in: [ID!]
  id_not_in: [ID!]
  id_lt: ID
  id_lte: ID
  id_gt: ID
  id_gte: ID
  id_contains: ID
  id_not_contains: ID
  id_starts_with: ID
  id_not_starts_with: ID
  id_ends_with: ID
  id_not_ends_with: ID
  company: CompanyWhereInput
  roles_every: RoleWhereInput
  roles_some: RoleWhereInput
  roles_none: RoleWhereInput
  name: String
  name_not: String
  name_in: [String!]
  name_not_in: [String!]
  name_lt: String
  name_lte: String
  name_gt: String
  name_gte: String
  name_contains: String
  name_not_contains: String
  name_starts_with: String
  name_not_starts_with: String
  name_ends_with: String
  name_not_ends_with: String
  title: String
  title_not: String
  title_in: [String!]
  title_not_in: [String!]
  title_lt: String
  title_lte: String
  title_gt: String
  title_gte: String
  title_contains: String
  title_not_contains: String
  title_starts_with: String
  title_not_starts_with: String
  title_ends_with: String
  title_not_ends_with: String
  description: String
  description_not: String
  description_in: [String!]
  description_not_in: [String!]
  description_lt: String
  description_lte: String
  description_gt: String
  description_gte: String
  description_contains: String
  description_not_contains: String
  description_starts_with: String
  description_not_starts_with: String
  description_ends_with: String
  description_not_ends_with: String
  AND: [CourseWhereInput!]
  OR: [CourseWhereInput!]
  NOT: [CourseWhereInput!]
}

input CourseWhereUniqueInput {
  id: ID
}

scalar Long

type Mutation {
  createCompany(data: CompanyCreateInput!): Company!
  updateCompany(data: CompanyUpdateInput!, where: CompanyWhereUniqueInput!): Company
  updateManyCompanies(data: CompanyUpdateManyMutationInput!, where: CompanyWhereInput): BatchPayload!
  upsertCompany(where: CompanyWhereUniqueInput!, create: CompanyCreateInput!, update: CompanyUpdateInput!): Company!
  deleteCompany(where: CompanyWhereUniqueInput!): Company
  deleteManyCompanies(where: CompanyWhereInput): BatchPayload!
  createCourse(data: CourseCreateInput!): Course!
  updateCourse(data: CourseUpdateInput!, where: CourseWhereUniqueInput!): Course
  updateManyCourses(data: CourseUpdateManyMutationInput!, where: CourseWhereInput): BatchPayload!
  upsertCourse(where: CourseWhereUniqueInput!, create: CourseCreateInput!, update: CourseUpdateInput!): Course!
  deleteCourse(where: CourseWhereUniqueInput!): Course
  deleteManyCourses(where: CourseWhereInput): BatchPayload!
  createRole(data: RoleCreateInput!): Role!
  updateRole(data: RoleUpdateInput!, where: RoleWhereUniqueInput!): Role
  updateManyRoles(data: RoleUpdateManyMutationInput!, where: RoleWhereInput): BatchPayload!
  upsertRole(where: RoleWhereUniqueInput!, create: RoleCreateInput!, update: RoleUpdateInput!): Role!
  deleteRole(where: RoleWhereUniqueInput!): Role
  deleteManyRoles(where: RoleWhereInput): BatchPayload!
  createRule(data: RuleCreateInput!): Rule!
  updateRule(data: RuleUpdateInput!, where: RuleWhereUniqueInput!): Rule
  updateManyRules(data: RuleUpdateManyMutationInput!, where: RuleWhereInput): BatchPayload!
  upsertRule(where: RuleWhereUniqueInput!, create: RuleCreateInput!, update: RuleUpdateInput!): Rule!
  deleteRule(where: RuleWhereUniqueInput!): Rule
  deleteManyRules(where: RuleWhereInput): BatchPayload!
  createUser(data: UserCreateInput!): User!
  updateUser(data: UserUpdateInput!, where: UserWhereUniqueInput!): User
  updateManyUsers(data: UserUpdateManyMutationInput!, where: UserWhereInput): BatchPayload!
  upsertUser(where: UserWhereUniqueInput!, create: UserCreateInput!, update: UserUpdateInput!): User!
  deleteUser(where: UserWhereUniqueInput!): User
  deleteManyUsers(where: UserWhereInput): BatchPayload!
}

enum MutationType {
  CREATED
  UPDATED
  DELETED
}

interface Node {
  id: ID!
}

type PageInfo {
  hasNextPage: Boolean!
  hasPreviousPage: Boolean!
  startCursor: String
  endCursor: String
}

type Query {
  company(where: CompanyWhereUniqueInput!): Company
  companies(where: CompanyWhereInput, orderBy: CompanyOrderByInput, skip: Int, after: String, before: String, first: Int, last: Int): [Company]!
  companiesConnection(where: CompanyWhereInput, orderBy: CompanyOrderByInput, skip: Int, after: String, before: String, first: Int, last: Int): CompanyConnection!
  course(where: CourseWhereUniqueInput!): Course
  courses(where: CourseWhereInput, orderBy: CourseOrderByInput, skip: Int, after: String, before: String, first: Int, last: Int): [Course]!
  coursesConnection(where: CourseWhereInput, orderBy: CourseOrderByInput, skip: Int, after: String, before: String, first: Int, last: Int): CourseConnection!
  role(where: RoleWhereUniqueInput!): Role
  roles(where: RoleWhereInput, orderBy: RoleOrderByInput, skip: Int, after: String, before: String, first: Int, last: Int): [Role]!
  rolesConnection(where: RoleWhereInput, orderBy: RoleOrderByInput, skip: Int, after: String, before: String, first: Int, last: Int): RoleConnection!
  rule(where: RuleWhereUniqueInput!): Rule
  rules(where: RuleWhereInput, orderBy: RuleOrderByInput, skip: Int, after: String, before: String, first: Int, last: Int): [Rule]!
  rulesConnection(where: RuleWhereInput, orderBy: RuleOrderByInput, skip: Int, after: String, before: String, first: Int, last: Int): RuleConnection!
  user(where: UserWhereUniqueInput!): User
  users(where: UserWhereInput, orderBy: UserOrderByInput, skip: Int, after: String, before: String, first: Int, last: Int): [User]!
  usersConnection(where: UserWhereInput, orderBy: UserOrderByInput, skip: Int, after: String, before: String, first: Int, last: Int): UserConnection!
  node(id: ID!): Node
}

type Role {
  id: ID!
  company: Company
  course: Course
  users(where: UserWhereInput, orderBy: UserOrderByInput, skip: Int, after: String, before: String, first: Int, last: Int): [User!]
  rules(where: RuleWhereInput, orderBy: RuleOrderByInput, skip: Int, after: String, before: String, first: Int, last: Int): [Rule!]
  title: String
}

type RoleConnection {
  pageInfo: PageInfo!
  edges: [RoleEdge]!
  aggregate: AggregateRole!
}

input RoleCreateInput {
  company: CompanyCreateOneWithoutRolesInput
  course: CourseCreateOneWithoutRolesInput
  users: UserCreateManyWithoutRolesInput
  rules: RuleCreateManyWithoutRoleInput
  title: String
}

input RoleCreateManyWithoutCompanyInput {
  create: [RoleCreateWithoutCompanyInput!]
  connect: [RoleWhereUniqueInput!]
}

input RoleCreateManyWithoutCourseInput {
  create: [RoleCreateWithoutCourseInput!]
  connect: [RoleWhereUniqueInput!]
}

input RoleCreateManyWithoutUsersInput {
  create: [RoleCreateWithoutUsersInput!]
  connect: [RoleWhereUniqueInput!]
}

input RoleCreateOneWithoutRulesInput {
  create: RoleCreateWithoutRulesInput
  connect: RoleWhereUniqueInput
}

input RoleCreateWithoutCompanyInput {
  course: CourseCreateOneWithoutRolesInput
  users: UserCreateManyWithoutRolesInput
  rules: RuleCreateManyWithoutRoleInput
  title: String
}

input RoleCreateWithoutCourseInput {
  company: CompanyCreateOneWithoutRolesInput
  users: UserCreateManyWithoutRolesInput
  rules: RuleCreateManyWithoutRoleInput
  title: String
}

input RoleCreateWithoutRulesInput {
  company: CompanyCreateOneWithoutRolesInput
  course: CourseCreateOneWithoutRolesInput
  users: UserCreateManyWithoutRolesInput
  title: String
}

input RoleCreateWithoutUsersInput {
  company: CompanyCreateOneWithoutRolesInput
  course: CourseCreateOneWithoutRolesInput
  rules: RuleCreateManyWithoutRoleInput
  title: String
}

type RoleEdge {
  node: Role!
  cursor: String!
}

enum RoleOrderByInput {
  id_ASC
  id_DESC
  title_ASC
  title_DESC
  createdAt_ASC
  createdAt_DESC
  updatedAt_ASC
  updatedAt_DESC
}

type RolePreviousValues {
  id: ID!
  title: String
}

input RoleScalarWhereInput {
  id: ID
  id_not: ID
  id_in: [ID!]
  id_not_in: [ID!]
  id_lt: ID
  id_lte: ID
  id_gt: ID
  id_gte: ID
  id_contains: ID
  id_not_contains: ID
  id_starts_with: ID
  id_not_starts_with: ID
  id_ends_with: ID
  id_not_ends_with: ID
  title: String
  title_not: String
  title_in: [String!]
  title_not_in: [String!]
  title_lt: String
  title_lte: String
  title_gt: String
  title_gte: String
  title_contains: String
  title_not_contains: String
  title_starts_with: String
  title_not_starts_with: String
  title_ends_with: String
  title_not_ends_with: String
  AND: [RoleScalarWhereInput!]
  OR: [RoleScalarWhereInput!]
  NOT: [RoleScalarWhereInput!]
}

type RoleSubscriptionPayload {
  mutation: MutationType!
  node: Role
  updatedFields: [String!]
  previousValues: RolePreviousValues
}

input RoleSubscriptionWhereInput {
  mutation_in: [MutationType!]
  updatedFields_contains: String
  updatedFields_contains_every: [String!]
  updatedFields_contains_some: [String!]
  node: RoleWhereInput
  AND: [RoleSubscriptionWhereInput!]
  OR: [RoleSubscriptionWhereInput!]
  NOT: [RoleSubscriptionWhereInput!]
}

input RoleUpdateInput {
  company: CompanyUpdateOneWithoutRolesInput
  course: CourseUpdateOneWithoutRolesInput
  users: UserUpdateManyWithoutRolesInput
  rules: RuleUpdateManyWithoutRoleInput
  title: String
}

input RoleUpdateManyDataInput {
  title: String
}

input RoleUpdateManyMutationInput {
  title: String
}

input RoleUpdateManyWithoutCompanyInput {
  create: [RoleCreateWithoutCompanyInput!]
  delete: [RoleWhereUniqueInput!]
  connect: [RoleWhereUniqueInput!]
  set: [RoleWhereUniqueInput!]
  disconnect: [RoleWhereUniqueInput!]
  update: [RoleUpdateWithWhereUniqueWithoutCompanyInput!]
  upsert: [RoleUpsertWithWhereUniqueWithoutCompanyInput!]
  deleteMany: [RoleScalarWhereInput!]
  updateMany: [RoleUpdateManyWithWhereNestedInput!]
}

input RoleUpdateManyWithoutCourseInput {
  create: [RoleCreateWithoutCourseInput!]
  delete: [RoleWhereUniqueInput!]
  connect: [RoleWhereUniqueInput!]
  set: [RoleWhereUniqueInput!]
  disconnect: [RoleWhereUniqueInput!]
  update: [RoleUpdateWithWhereUniqueWithoutCourseInput!]
  upsert: [RoleUpsertWithWhereUniqueWithoutCourseInput!]
  deleteMany: [RoleScalarWhereInput!]
  updateMany: [RoleUpdateManyWithWhereNestedInput!]
}

input RoleUpdateManyWithoutUsersInput {
  create: [RoleCreateWithoutUsersInput!]
  delete: [RoleWhereUniqueInput!]
  connect: [RoleWhereUniqueInput!]
  set: [RoleWhereUniqueInput!]
  disconnect: [RoleWhereUniqueInput!]
  update: [RoleUpdateWithWhereUniqueWithoutUsersInput!]
  upsert: [RoleUpsertWithWhereUniqueWithoutUsersInput!]
  deleteMany: [RoleScalarWhereInput!]
  updateMany: [RoleUpdateManyWithWhereNestedInput!]
}

input RoleUpdateManyWithWhereNestedInput {
  where: RoleScalarWhereInput!
  data: RoleUpdateManyDataInput!
}

input RoleUpdateOneRequiredWithoutRulesInput {
  create: RoleCreateWithoutRulesInput
  update: RoleUpdateWithoutRulesDataInput
  upsert: RoleUpsertWithoutRulesInput
  connect: RoleWhereUniqueInput
}

input RoleUpdateWithoutCompanyDataInput {
  course: CourseUpdateOneWithoutRolesInput
  users: UserUpdateManyWithoutRolesInput
  rules: RuleUpdateManyWithoutRoleInput
  title: String
}

input RoleUpdateWithoutCourseDataInput {
  company: CompanyUpdateOneWithoutRolesInput
  users: UserUpdateManyWithoutRolesInput
  rules: RuleUpdateManyWithoutRoleInput
  title: String
}

input RoleUpdateWithoutRulesDataInput {
  company: CompanyUpdateOneWithoutRolesInput
  course: CourseUpdateOneWithoutRolesInput
  users: UserUpdateManyWithoutRolesInput
  title: String
}

input RoleUpdateWithoutUsersDataInput {
  company: CompanyUpdateOneWithoutRolesInput
  course: CourseUpdateOneWithoutRolesInput
  rules: RuleUpdateManyWithoutRoleInput
  title: String
}

input RoleUpdateWithWhereUniqueWithoutCompanyInput {
  where: RoleWhereUniqueInput!
  data: RoleUpdateWithoutCompanyDataInput!
}

input RoleUpdateWithWhereUniqueWithoutCourseInput {
  where: RoleWhereUniqueInput!
  data: RoleUpdateWithoutCourseDataInput!
}

input RoleUpdateWithWhereUniqueWithoutUsersInput {
  where: RoleWhereUniqueInput!
  data: RoleUpdateWithoutUsersDataInput!
}

input RoleUpsertWithoutRulesInput {
  update: RoleUpdateWithoutRulesDataInput!
  create: RoleCreateWithoutRulesInput!
}

input RoleUpsertWithWhereUniqueWithoutCompanyInput {
  where: RoleWhereUniqueInput!
  update: RoleUpdateWithoutCompanyDataInput!
  create: RoleCreateWithoutCompanyInput!
}

input RoleUpsertWithWhereUniqueWithoutCourseInput {
  where: RoleWhereUniqueInput!
  update: RoleUpdateWithoutCourseDataInput!
  create: RoleCreateWithoutCourseInput!
}

input RoleUpsertWithWhereUniqueWithoutUsersInput {
  where: RoleWhereUniqueInput!
  update: RoleUpdateWithoutUsersDataInput!
  create: RoleCreateWithoutUsersInput!
}

input RoleWhereInput {
  id: ID
  id_not: ID
  id_in: [ID!]
  id_not_in: [ID!]
  id_lt: ID
  id_lte: ID
  id_gt: ID
  id_gte: ID
  id_contains: ID
  id_not_contains: ID
  id_starts_with: ID
  id_not_starts_with: ID
  id_ends_with: ID
  id_not_ends_with: ID
  company: CompanyWhereInput
  course: CourseWhereInput
  users_every: UserWhereInput
  users_some: UserWhereInput
  users_none: UserWhereInput
  rules_every: RuleWhereInput
  rules_some: RuleWhereInput
  rules_none: RuleWhereInput
  title: String
  title_not: String
  title_in: [String!]
  title_not_in: [String!]
  title_lt: String
  title_lte: String
  title_gt: String
  title_gte: String
  title_contains: String
  title_not_contains: String
  title_starts_with: String
  title_not_starts_with: String
  title_ends_with: String
  title_not_ends_with: String
  AND: [RoleWhereInput!]
  OR: [RoleWhereInput!]
  NOT: [RoleWhereInput!]
}

input RoleWhereUniqueInput {
  id: ID
}

type Rule {
  id: ID!
  role: Role!
  name: RuleName
}

type RuleConnection {
  pageInfo: PageInfo!
  edges: [RuleEdge]!
  aggregate: AggregateRule!
}

input RuleCreateInput {
  role: RoleCreateOneWithoutRulesInput!
  name: RuleName
}

input RuleCreateManyWithoutRoleInput {
  create: [RuleCreateWithoutRoleInput!]
  connect: [RuleWhereUniqueInput!]
}

input RuleCreateWithoutRoleInput {
  name: RuleName
}

type RuleEdge {
  node: Rule!
  cursor: String!
}

enum RuleName {
  COMPANY_OWNER
  COMPANY_COURSE_CREATE
  COURSE_VIEW
  COURSE_EDIT
  COURSE_DELETE
}

enum RuleOrderByInput {
  id_ASC
  id_DESC
  name_ASC
  name_DESC
  createdAt_ASC
  createdAt_DESC
  updatedAt_ASC
  updatedAt_DESC
}

type RulePreviousValues {
  id: ID!
  name: RuleName
}

input RuleScalarWhereInput {
  id: ID
  id_not: ID
  id_in: [ID!]
  id_not_in: [ID!]
  id_lt: ID
  id_lte: ID
  id_gt: ID
  id_gte: ID
  id_contains: ID
  id_not_contains: ID
  id_starts_with: ID
  id_not_starts_with: ID
  id_ends_with: ID
  id_not_ends_with: ID
  name: RuleName
  name_not: RuleName
  name_in: [RuleName!]
  name_not_in: [RuleName!]
  AND: [RuleScalarWhereInput!]
  OR: [RuleScalarWhereInput!]
  NOT: [RuleScalarWhereInput!]
}

type RuleSubscriptionPayload {
  mutation: MutationType!
  node: Rule
  updatedFields: [String!]
  previousValues: RulePreviousValues
}

input RuleSubscriptionWhereInput {
  mutation_in: [MutationType!]
  updatedFields_contains: String
  updatedFields_contains_every: [String!]
  updatedFields_contains_some: [String!]
  node: RuleWhereInput
  AND: [RuleSubscriptionWhereInput!]
  OR: [RuleSubscriptionWhereInput!]
  NOT: [RuleSubscriptionWhereInput!]
}

input RuleUpdateInput {
  role: RoleUpdateOneRequiredWithoutRulesInput
  name: RuleName
}

input RuleUpdateManyDataInput {
  name: RuleName
}

input RuleUpdateManyMutationInput {
  name: RuleName
}

input RuleUpdateManyWithoutRoleInput {
  create: [RuleCreateWithoutRoleInput!]
  delete: [RuleWhereUniqueInput!]
  connect: [RuleWhereUniqueInput!]
  set: [RuleWhereUniqueInput!]
  disconnect: [RuleWhereUniqueInput!]
  update: [RuleUpdateWithWhereUniqueWithoutRoleInput!]
  upsert: [RuleUpsertWithWhereUniqueWithoutRoleInput!]
  deleteMany: [RuleScalarWhereInput!]
  updateMany: [RuleUpdateManyWithWhereNestedInput!]
}

input RuleUpdateManyWithWhereNestedInput {
  where: RuleScalarWhereInput!
  data: RuleUpdateManyDataInput!
}

input RuleUpdateWithoutRoleDataInput {
  name: RuleName
}

input RuleUpdateWithWhereUniqueWithoutRoleInput {
  where: RuleWhereUniqueInput!
  data: RuleUpdateWithoutRoleDataInput!
}

input RuleUpsertWithWhereUniqueWithoutRoleInput {
  where: RuleWhereUniqueInput!
  update: RuleUpdateWithoutRoleDataInput!
  create: RuleCreateWithoutRoleInput!
}

input RuleWhereInput {
  id: ID
  id_not: ID
  id_in: [ID!]
  id_not_in: [ID!]
  id_lt: ID
  id_lte: ID
  id_gt: ID
  id_gte: ID
  id_contains: ID
  id_not_contains: ID
  id_starts_with: ID
  id_not_starts_with: ID
  id_ends_with: ID
  id_not_ends_with: ID
  role: RoleWhereInput
  name: RuleName
  name_not: RuleName
  name_in: [RuleName!]
  name_not_in: [RuleName!]
  AND: [RuleWhereInput!]
  OR: [RuleWhereInput!]
  NOT: [RuleWhereInput!]
}

input RuleWhereUniqueInput {
  id: ID
}

type Subscription {
  company(where: CompanySubscriptionWhereInput): CompanySubscriptionPayload
  course(where: CourseSubscriptionWhereInput): CourseSubscriptionPayload
  role(where: RoleSubscriptionWhereInput): RoleSubscriptionPayload
  rule(where: RuleSubscriptionWhereInput): RuleSubscriptionPayload
  user(where: UserSubscriptionWhereInput): UserSubscriptionPayload
}

type User {
  id: ID!
  email: String!
  password: String
  code: String
  name: String!
  roles(where: RoleWhereInput, orderBy: RoleOrderByInput, skip: Int, after: String, before: String, first: Int, last: Int): [Role!]
}

type UserConnection {
  pageInfo: PageInfo!
  edges: [UserEdge]!
  aggregate: AggregateUser!
}

input UserCreateInput {
  email: String!
  password: String
  code: String
  name: String!
  roles: RoleCreateManyWithoutUsersInput
}

input UserCreateManyWithoutRolesInput {
  create: [UserCreateWithoutRolesInput!]
  connect: [UserWhereUniqueInput!]
}

input UserCreateWithoutRolesInput {
  email: String!
  password: String
  code: String
  name: String!
}

type UserEdge {
  node: User!
  cursor: String!
}

enum UserOrderByInput {
  id_ASC
  id_DESC
  email_ASC
  email_DESC
  password_ASC
  password_DESC
  code_ASC
  code_DESC
  name_ASC
  name_DESC
  createdAt_ASC
  createdAt_DESC
  updatedAt_ASC
  updatedAt_DESC
}

type UserPreviousValues {
  id: ID!
  email: String!
  password: String
  code: String
  name: String!
}

input UserScalarWhereInput {
  id: ID
  id_not: ID
  id_in: [ID!]
  id_not_in: [ID!]
  id_lt: ID
  id_lte: ID
  id_gt: ID
  id_gte: ID
  id_contains: ID
  id_not_contains: ID
  id_starts_with: ID
  id_not_starts_with: ID
  id_ends_with: ID
  id_not_ends_with: ID
  email: String
  email_not: String
  email_in: [String!]
  email_not_in: [String!]
  email_lt: String
  email_lte: String
  email_gt: String
  email_gte: String
  email_contains: String
  email_not_contains: String
  email_starts_with: String
  email_not_starts_with: String
  email_ends_with: String
  email_not_ends_with: String
  password: String
  password_not: String
  password_in: [String!]
  password_not_in: [String!]
  password_lt: String
  password_lte: String
  password_gt: String
  password_gte: String
  password_contains: String
  password_not_contains: String
  password_starts_with: String
  password_not_starts_with: String
  password_ends_with: String
  password_not_ends_with: String
  code: String
  code_not: String
  code_in: [String!]
  code_not_in: [String!]
  code_lt: String
  code_lte: String
  code_gt: String
  code_gte: String
  code_contains: String
  code_not_contains: String
  code_starts_with: String
  code_not_starts_with: String
  code_ends_with: String
  code_not_ends_with: String
  name: String
  name_not: String
  name_in: [String!]
  name_not_in: [String!]
  name_lt: String
  name_lte: String
  name_gt: String
  name_gte: String
  name_contains: String
  name_not_contains: String
  name_starts_with: String
  name_not_starts_with: String
  name_ends_with: String
  name_not_ends_with: String
  AND: [UserScalarWhereInput!]
  OR: [UserScalarWhereInput!]
  NOT: [UserScalarWhereInput!]
}

type UserSubscriptionPayload {
  mutation: MutationType!
  node: User
  updatedFields: [String!]
  previousValues: UserPreviousValues
}

input UserSubscriptionWhereInput {
  mutation_in: [MutationType!]
  updatedFields_contains: String
  updatedFields_contains_every: [String!]
  updatedFields_contains_some: [String!]
  node: UserWhereInput
  AND: [UserSubscriptionWhereInput!]
  OR: [UserSubscriptionWhereInput!]
  NOT: [UserSubscriptionWhereInput!]
}

input UserUpdateInput {
  email: String
  password: String
  code: String
  name: String
  roles: RoleUpdateManyWithoutUsersInput
}

input UserUpdateManyDataInput {
  email: String
  password: String
  code: String
  name: String
}

input UserUpdateManyMutationInput {
  email: String
  password: String
  code: String
  name: String
}

input UserUpdateManyWithoutRolesInput {
  create: [UserCreateWithoutRolesInput!]
  delete: [UserWhereUniqueInput!]
  connect: [UserWhereUniqueInput!]
  set: [UserWhereUniqueInput!]
  disconnect: [UserWhereUniqueInput!]
  update: [UserUpdateWithWhereUniqueWithoutRolesInput!]
  upsert: [UserUpsertWithWhereUniqueWithoutRolesInput!]
  deleteMany: [UserScalarWhereInput!]
  updateMany: [UserUpdateManyWithWhereNestedInput!]
}

input UserUpdateManyWithWhereNestedInput {
  where: UserScalarWhereInput!
  data: UserUpdateManyDataInput!
}

input UserUpdateWithoutRolesDataInput {
  email: String
  password: String
  code: String
  name: String
}

input UserUpdateWithWhereUniqueWithoutRolesInput {
  where: UserWhereUniqueInput!
  data: UserUpdateWithoutRolesDataInput!
}

input UserUpsertWithWhereUniqueWithoutRolesInput {
  where: UserWhereUniqueInput!
  update: UserUpdateWithoutRolesDataInput!
  create: UserCreateWithoutRolesInput!
}

input UserWhereInput {
  id: ID
  id_not: ID
  id_in: [ID!]
  id_not_in: [ID!]
  id_lt: ID
  id_lte: ID
  id_gt: ID
  id_gte: ID
  id_contains: ID
  id_not_contains: ID
  id_starts_with: ID
  id_not_starts_with: ID
  id_ends_with: ID
  id_not_ends_with: ID
  email: String
  email_not: String
  email_in: [String!]
  email_not_in: [String!]
  email_lt: String
  email_lte: String
  email_gt: String
  email_gte: String
  email_contains: String
  email_not_contains: String
  email_starts_with: String
  email_not_starts_with: String
  email_ends_with: String
  email_not_ends_with: String
  password: String
  password_not: String
  password_in: [String!]
  password_not_in: [String!]
  password_lt: String
  password_lte: String
  password_gt: String
  password_gte: String
  password_contains: String
  password_not_contains: String
  password_starts_with: String
  password_not_starts_with: String
  password_ends_with: String
  password_not_ends_with: String
  code: String
  code_not: String
  code_in: [String!]
  code_not_in: [String!]
  code_lt: String
  code_lte: String
  code_gt: String
  code_gte: String
  code_contains: String
  code_not_contains: String
  code_starts_with: String
  code_not_starts_with: String
  code_ends_with: String
  code_not_ends_with: String
  name: String
  name_not: String
  name_in: [String!]
  name_not_in: [String!]
  name_lt: String
  name_lte: String
  name_gt: String
  name_gte: String
  name_contains: String
  name_not_contains: String
  name_starts_with: String
  name_not_starts_with: String
  name_ends_with: String
  name_not_ends_with: String
  roles_every: RoleWhereInput
  roles_some: RoleWhereInput
  roles_none: RoleWhereInput
  AND: [UserWhereInput!]
  OR: [UserWhereInput!]
  NOT: [UserWhereInput!]
}

input UserWhereUniqueInput {
  id: ID
  email: String
}
`
