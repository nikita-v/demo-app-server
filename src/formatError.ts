import { ApolloError, formatError, isInstance } from 'apollo-errors'

export default (error: any) =>
  formatError(
    isInstance(error.originalError)
      ? error
      : new ApolloError(
          'SERVER_ERROR',
          { message: error.message },
          { message: '' },
        ),
  )
